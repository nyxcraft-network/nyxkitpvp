package net.nyxcraft.dev.kitpvp;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import net.nyxcraft.dev.kitpvp.game.GameManager;
import net.nyxcraft.dev.kitpvp.game.MapRotation;
import net.nyxcraft.dev.kitpvp.game.config.GameConfig;
import net.nyxcraft.dev.kitpvp.game.listener.MiscListener;
import net.nyxcraft.dev.kitpvp.game.listener.PlayerListener;
import net.nyxcraft.dev.kitpvp.game.listener.StateListener;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Main plugin class for the {@link KitPVP} game type.
 * 
 * @author Gideon
 */
public class KitPVP extends JavaPlugin {

    private static final List<Supplier<? extends Listener>> default_listeners = Arrays.<Supplier<? extends Listener>> asList(
            StateListener::new, PlayerListener::new, MiscListener::new
    );
    private static final String CONFIG_FILE = "config.json";
    private static KitPVP instance = null;
    
    private final GameConfig config;
//    private KitPVPDatabaseManager dbman;
//    private KitPVPData data;
    private MapRotation maps;
    
    public KitPVP() {
        this.config = GameConfig.load(getConfigurationFile(), GameConfig.class);
//        this.dbman = null;
//        this.data = null;
        this.maps = null;
    }
    
    public void onEnable() {
        KitPVP.instance = this;
        GameManager.getInstance();
        
//        this.dbman = new KitPVPDatabaseManager();
//        this.data = KitPVPDataDAO.getInstance().initData();
        this.maps = new MapRotation(config);
        
        KitPVP.default_listeners.stream().map(s -> s.get()).forEachOrdered(this::register);
        
        maps.init();
    }
    
    private void register(Listener l) {
        Bukkit.getPluginManager().registerEvents(l, this);
    }
    
    public void onDisable() {
        HandlerList.unregisterAll(this);
        KitPVP.instance = null;
    }
    
    public boolean isSufficient() {
        return config != null && maps != null && maps.getMapCount() > 0 && maps.hasCurrentMap();
    }
    
    public GameConfig getConfiguration() {
        return config;
    }
    
//    public KitPVPDatabaseManager getDatabaseManager() {
//        return dbman;
//    }
//    public KitPVPData getKitPVPData() {
//        return data;
//    }
    
    public MapRotation getMapRotation() {
        return maps;
    }
    
    public File getConfigurationFile() {
        return new File(getDataFolder(), CONFIG_FILE);
    }
    
    
    public static KitPVP getInstance() {
        return instance;
    }
}
