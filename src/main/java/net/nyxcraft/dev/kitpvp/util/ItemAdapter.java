package net.nyxcraft.dev.kitpvp.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import net.nyxcraft.dev.nyxcore.ui.MenuItem;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

/**
 * An interface for denoting an Object that can be interpreted as both an
 * ItemStack and a MenuItem.
 *
 * @author Gideon
 */
public interface ItemAdapter extends Supplier<ItemStack> {

    /**
     * Get the ordinal of this ItemAdapter, which should be usable as a unique
     * inventory slot that the ItemStack represented by this ItemAdapter can be
     * placed in.
     *
     * @return The ordinal index of this ItemAdapater.
     */
    public int ordinal();

    /**
     * Get the inventory slot index for the ItemStack represented by this
     * ItemAdapter, the default implementation of which is the same as the
     * {@link #ordinal()} method.
     * <p>
     * This method is designed with Enum implementations in mind, so that any
     * given constant may override this method if an index other than that
     * constant's ordinal is desired.
     *
     * @return The inventory index for this ItemAdapter.
     */
    public default int index() {
        return ordinal();
    }

    /**
     * Get the name that should be used as the display of this ItemStack.
     *
     * @return The display name for the ItemStack.
     */
    public String getDisplay();

    /**
     * Get the String[] that should be used for the lore of this ItemStack.
     *
     * @return The String[] for the lore.
     */
    public String[] getLore();

    /**
     * Get the quantity that should be used for this ItemStack.
     *
     * @return The quantity of the ItemStack.
     */
    public int getQuantity();

    /**
     * Get the Material that should be used for this ItemStack.
     *
     * @return The Material for this ItemStack.
     */
    public Material getMaterial();

    /**
     * Get the data for this ItemStack, can be null to indicate that the
     * getDurability() method should be used.
     * <p>
     * If both this method and the getDurability method return null, 0 is
     * implied.
     *
     * @return The Byte data, possibly null.
     */
    public Byte getData();

    /**
     * Get the durability for this ItemStack, can be null to indicate that the
     * getData() method should be used.
     * <p>
     * If both this method and the getData() method return null, 0 is implied.
     *
     * @return The Short durability, possibly null.
     */
    public Short getDurability();

    /**
     * Handle a click action for the given Player.
     *
     * @param player The Player to handle clicking for.
     */
    public void onClick(Player player);

    /**
     * Handle a click action for the given Player using the given ClickType.
     * <p>
     * This method's default implementation calls {@link #onClick(Player)}.
     *
     * @param player The Player to handle clicking for.
     * @param click The ClickType that should be assumed for the action.
     */
    public default void onClick(Player player, ClickType click) {
        onClick(player);
    }

    /**
     * Get the MaterialData for this ItemStack, as given by the getMaterial()
     * method and inferred through the getData() and getDurability() methods.
     *
     * @return The MaterialData to represent this ItemStack.
     */
    @SuppressWarnings("deprecation")
    public default MaterialData getMaterialData() {
        Material material = getMaterial();
        Byte data = getData();

        if (material == null) {
            throw new IllegalArgumentException("The Material cannot be null!");
        }

        if (data == null) {
            return new MaterialData(material);
        } else {
            return new MaterialData(material, data);
        }
    }

    /**
     * Get this ItemStack's lore as a List instead of an array.
     *
     * @return The List of lore, empty if getLore() returned null.
     */
    public default List<String> getLoreList() {
        String[] lore = getLore();

        return lore == null ? new ArrayList<>() : Arrays.asList(lore);
    }

    /**
     * Get the ItemStack represented by this MenuItemAdapter.
     *
     * @return The ItemStack.
     */
    public default ItemStack get() {
        ItemStack base = getMaterialData().toItemStack(getQuantity());
        ItemMeta meta = base.getItemMeta();
        String display = getDisplay();
        String[] lore = getLore();

        if (meta != null && display != null) {
            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',display));
        }

        if (meta != null && lore != null && lore.length > 0) {
            meta.setLore(Arrays.asList(lore).stream()
                    .filter(Objects::nonNull)
                    .map(line -> ChatColor.translateAlternateColorCodes('&', line))
                    .collect(Collectors.toList()));
        }

        if (meta != null) {
            base.setItemMeta(meta);
        }
        
        return base;
    }

    /**
     * Get the MenuItem represented by this MenuItemAdapter.
     *
     * @return The MenuItem.
     */
    public default MenuItemWrapper toMenuItem() {
        return new MenuItemWrapper(this);
    }

    /**
     * A MenuItem implementation that is used to wrap a MenuItemAdapter to
     * provide the functionality of a MenuItem.
     */
    public static class MenuItemWrapper extends MenuItem {

        final ItemAdapter item;

        MenuItemWrapper(ItemAdapter item) {
            super(item.getDisplay(), item.getMaterialData(), item.getQuantity(), asDurabilityFor(item));

            this.item = item;
            this.setData(asDurabilityFor(item));
            this.setDescriptions(item.getLoreList());
        }

        public void onClick(Player player) {
            item.onClick(player);
        }

        public void onClick(Player player, ClickType click) {
            item.onClick(player, click);
        }

        public static short asDurabilityFor(ItemAdapter item) {
            Short dura = item.getDurability();
            Byte data = item.getData();

            if (dura != null) {
                return dura.shortValue();
            } else if (data != null) {
                return data.shortValue();
            } else {
                return 0;
            }
        }
    }

    /**
     * Create an ItemAdapter that represents the given ItemStack, using the
     * given action for clicks, and using the index of -1.
     * <p>
     * The given Consumer will be invoked with null as the ClickType when a call
     * to the method {@link #onClick(Player)} is made.
     */
    public static ItemAdapter of(ItemStack item, BiConsumer<Player, ClickType> action) {
        return of(item, action, -1);
    }

    /**
     * Create an ItemAdapter that represents the given ItemStack, using the
     * given action for clicks, and using the given index.
     * <p>
     * The given Consumer will be invoked with null as the ClickType when a call
     * to the method {@link #onClick(Player)} is made.
     */
    public static ItemAdapter of(ItemStack item, BiConsumer<Player, ClickType> action, int index) {
        return of(item, action, null, index);
    }

    /**
     * Create an ItemAdapter that represents the given ItemStack, using the
     * given action for clicks, and using the index of -1.
     * <p>
     * The given Consumer will be invoked with the given ClickType when a call
     * to the method {@link #onClick(Player)} is made.
     */
    public static ItemAdapter of(ItemStack item, BiConsumer<Player, ClickType> action, ClickType defclick) {
        return of(item, action, defclick, -1);
    }

    /**
     * Create an ItemAdapter that represents the given ItemStack, using the
     * given action for clicks, and using the given index.
     * <p>
     * The given Consumer will be invoked with the given ClickType when a call
     * to the method {@link #onClick(Player)} is made.
     */
    public static ItemAdapter of(ItemStack item, BiConsumer<Player, ClickType> action, ClickType defclick, int index) {
        return new ItemAdapterImpl(item, action, defclick, index);
    }

    /**
     * Create an ItemAdapter that represents the given ItemStack, using the
     * given action for clicks, and using the index of -1.
     * <p>
     * The given Consumer will also be invoked, and the ClickType argument
     * ignored, when a call to {@link #onClick(Player, ClickType)} is made.
     */
    public static ItemAdapter of(ItemStack item, Consumer<Player> action) {
        return of(item, action, -1);
    }

    /**
     * Create an ItemAdapter that represents the given ItemStack, using the
     * given action for clicks, and using the given index.
     * <p>
     * The given Consumer will also be invoked, and the ClickType argument
     * ignored, when a call to {@link #onClick(Player, ClickType)} is made.
     */
    public static ItemAdapter of(ItemStack item, Consumer<Player> action, int index) {
        return new ItemAdapterImpl(item, action, index);
    }
}
