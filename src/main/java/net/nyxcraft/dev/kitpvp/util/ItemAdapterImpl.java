package net.nyxcraft.dev.kitpvp.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

/*
 * A basic implementation of ItemAdapter.
 */
class ItemAdapterImpl implements ItemAdapter {

    final Consumer<Player> action_woclick;
    final BiConsumer<Player, ClickType> action_wclick;
    final ItemStack item;
    final int index;

    ItemAdapterImpl(ItemStack item, BiConsumer<Player, ClickType> action, ClickType defclick, int index) {
        this.action_woclick = action == null ? null : player -> action.accept(player, defclick);
        this.action_wclick = action;
        this.item = Objects.requireNonNull(item, "An ItemAdapter's ItemStack cannot be null!");
        this.index = index;
    }

    ItemAdapterImpl(ItemStack item, Consumer<Player> action, int index) {
        this.action_woclick = action;
        this.action_wclick = action == null ? null : (player, click) -> action.accept(player);
        this.item = Objects.requireNonNull(item, "An ItemAdapter's ItemStack cannot be null!");
        this.index = index;
    }

    public ItemStack get() {
        return item;
    }

    public int ordinal() {
        return index;
    }

    public String getDisplay() {
        if (item.getItemMeta() == null) {
            return "";
        } else if (item.getItemMeta().getDisplayName() == null) {
            return "";
        } else {
            return item.getItemMeta().getDisplayName();
        }
    }

    public String[] getLore() {
        List<String> lore = getLoreList();
        return lore.toArray(new String[lore.size()]);
    }

    public List<String> getLoreList() {
        if (item.getItemMeta() == null) {
            return new ArrayList<>();
        } else if (item.getItemMeta().getLore() == null) {
            return new ArrayList<>();
        } else {
            return item.getItemMeta().getLore();
        }
    }

    public int getQuantity() {
        return item.getAmount();
    }

    public Material getMaterial() {
        return item.getType();
    }

    @SuppressWarnings("deprecation")
    public Byte getData() {
        return item.getData().getData();
    }

    public Short getDurability() {
        return item.getDurability();
    }

    public void onClick(Player player) {
        if (action_woclick != null) {
            action_woclick.accept(player);
        }
    }

    public void onClick(Player player, ClickType click) {
        if (action_wclick != null) {
            action_wclick.accept(player, click);
        }
    }
}
