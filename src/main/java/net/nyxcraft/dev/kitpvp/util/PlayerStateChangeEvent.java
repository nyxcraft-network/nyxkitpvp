package net.nyxcraft.dev.kitpvp.util;

import javax.annotation.CheckForNull;

import net.nyxcraft.dev.kitpvp.game.GameManager.PlayerData;
import net.nyxcraft.dev.kitpvp.game.PlayerState;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

/**
 * An Event extension designed to be called when a player's state changes.
 * 
 * @author Gideon
 */
public class PlayerStateChangeEvent extends PlayerEvent {

    private static final HandlerList handlers = new HandlerList();
    
    private final PlayerData data;
    private final PlayerState to, from;
    
    public PlayerStateChangeEvent(PlayerData data, PlayerState to, PlayerState from) {
        super(data.getPlayer());
        
        this.data = data;
        this.to = to;
        this.from = from;
    }
    
    public PlayerData getData() {
        return data;
    }
    
    public PlayerState getTo(){
        return to;
    }
    
    @CheckForNull
    public PlayerState getFrom() {
        return from;
    }
    
    public HandlerList getHandlers() {
        return handlers;
    }
    
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    public static void call(PlayerData data, PlayerState to, PlayerState from) {
        Bukkit.getPluginManager().callEvent(new PlayerStateChangeEvent(data, to, from));
    }
}
