package net.nyxcraft.dev.kitpvp.game;

import com.google.common.collect.LinkedHashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import net.nyxcraft.dev.kitpvp.KitPVP;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

/**
 * Class for initializing and maintaining the game scoreboard.
 *
 * @author Gideon
 */
public class ScoreboardInstance {

    private static final Map<UUID, ScoreboardInstance> scoreboards = new HashMap<>();
    private static final Multiset<String> scores = LinkedHashMultiset.create();

    private UUID uuid;
    private Scoreboard scoreboard;
    private Objective objective;
    public final ScoreboardField MAPNAME = new ScoreboardField(9, ChatColor.GOLD + "Map Name:");
    public final ScoreboardField MAPAUTHOR = new ScoreboardField(7, ChatColor.GOLD + "Created By:");
    public final ScoreboardField LEADER = new ScoreboardField(5, ChatColor.GOLD + "MVP:");
    public final ScoreboardField TIME = new ScoreboardField(1, ChatColor.GOLD + "Time Left:");
    private final ScoreboardField KILLS = new ScoreboardField(3, ChatColor.GOLD + "KILLS");

    public ScoreboardInstance(Player player) {
        this.uuid = player.getUniqueId();
        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        this.objective = scoreboard.registerNewObjective("nyxcraft_sidebar", "dummy");

        init();
        showTo(player);
        scoreboards.put(player.getUniqueId(), this);
    }

    private void init() {
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(ChatColor.GOLD + "Kit PvP");

        clearField(MAPNAME);
        clearField(MAPAUTHOR);
        clearField(LEADER);
        clearField(TIME);

        setField(MAPNAME, KitPVP.getInstance().getMapRotation().getCurrentMap().getConfig().name);
        setField(MAPAUTHOR, KitPVP.getInstance().getMapRotation().getCurrentMap().getConfig().author);
        updateScore();
    }

    public static Multiset<String> getScores() {
        return scores;
    }

    public static void incrementScore(UUID uuid) {
        scores.add(Bukkit.getPlayer(uuid).getName());
        for (ScoreboardInstance instance : scoreboards.values()) {
            instance.updateScore();
        }
    }

    public void updateScore() {
        Iterator<String> it = Multisets.copyHighestCountFirst(scores).iterator();
        setField(LEADER, it.hasNext() ? it.next() : "Nobody");
        setField(KILLS, "" + scores.count(Bukkit.getPlayer(uuid).getName()));
    }

    public void showTo(Player player) {
        player.setScoreboard(scoreboard);
    }

    public void clearField(ScoreboardField field) {
        setField(field, null);
    }

    public void setField(ScoreboardField field, String value) {
        field.set(objective, value);
    }

    public static void cleanup(Player player) {
        scoreboards.remove(player.getUniqueId());
    }


    public static class ScoreboardField {

        private final int score;
        private final String key;
        private String lastvalue;

        private ScoreboardField(int score, String key) {
            this.score = score;
            this.key = key;
            this.lastvalue = null;
        }

        private void set(Objective objective, String value) {
            value = value == null ? null : value.length() > 16 ? value.substring(0, 16) : value;
            objective.getScore(key).setScore(kscore());

            if (lastvalue != null) {
                objective.getScoreboard().resetScores(lastvalue);
            }

            this.lastvalue = value;

            if (value != null) {
                objective.getScore(value).setScore(vscore());
            }
        }

        private int kscore() {
            return score;
        }

        private int vscore() {
            return score - 1;
        }
    }

    public static Map<UUID, ScoreboardInstance> getScoreboards() {
        return scoreboards;
    }
}