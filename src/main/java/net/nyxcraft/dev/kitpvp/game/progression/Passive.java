package net.nyxcraft.dev.kitpvp.game.progression;

import java.util.Set;
import java.util.WeakHashMap;
import java.util.function.Consumer;

import net.nyxcraft.dev.kitpvp.KitPVP;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

/**
 * A base class for a passive effect. A passive effect may require listeners or
 * tasks to maintain its features, and any such listeners or tasks are
 * registered whenever necessary, including during construction. If a Passive is
 * no longer required, the {@link #stop()} method will perform any necessary
 * cleanup.
 */
public abstract class Passive implements Listener {

    protected final Consumer<Player> activate, deactivate;
    protected final Set<Player> players;
    
    public Passive(Consumer<Player> activate, Consumer<Player> deactivate) {
        this.activate = activate;
        this.deactivate = deactivate;
        this.players = new WeakHashMap<Player, Void>().keySet();
        
        Bukkit.getPluginManager().registerEvents(this, KitPVP.getInstance());
    }
    
    public void stop() {
        // Overridden by subclasses as required
    }
    
    public final boolean active(Player player) {
        return players.contains(player);
    }
    
    public final void equip(Player player) {
        ProgressionItem.handle(player, activate);
    }
    
    public final void unequip(Player player) {
        ProgressionItem.handle(player, deactivate);
    }
}