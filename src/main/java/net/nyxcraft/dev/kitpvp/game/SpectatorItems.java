package net.nyxcraft.dev.kitpvp.game;

import java.util.function.Consumer;

import net.nyxcraft.dev.kitpvp.util.ItemAdapter;

import net.nyxcraft.dev.nyxcore.database.redis.NetworkUtil;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * An Enum of items for spectators to receive.
 * 
 * @author Gideon
 */
public enum SpectatorItems implements ItemAdapter {

    KIT(p -> {
        // TODO
        p.sendMessage("Generating random kit.");
        Kit.generateRandomKit(p);
    }, Material.CHAINMAIL_CHESTPLATE, ChatColor.YELLOW + "Choose Your Kit"),
    @SuppressWarnings("deprecation")
    GAME(p -> {
        GameManager.PlayerData data = GameManager.getInstance().getPlayerData(p);
        
        if (data != null && PlayerState.SPECTATING.equals(data.getState())) {
            data.setFighting();
        }
    }, Material.STAINED_CLAY, DyeColor.GREEN.getWoolData(), ChatColor.GREEN + "Enter the Fray"),
    STORE(p -> {
        // TODO
    }, Material.AIR/*Material.GOLD_BLOCK*/, ChatColor.AQUA + "View the Shop"),
    HUB(p -> NetworkUtil.send(p, "lobby"), Material.EYE_OF_ENDER, ChatColor.LIGHT_PURPLE + "Return to Hub");
    
    private final String display;
    private final String[] lore;
    private final Material material;
    private final Number data;
    private final int quantity;
    private final Consumer<Player> click;
    
    private SpectatorItems(Consumer<Player> click, Material material, Number data, int quantity, String display, String... lore) {
        this.click = click;
        this.material = material;
        this.data = data;
        this.quantity = quantity;
        this.display = display;
        this.lore = lore;
    }
    
    private SpectatorItems(Consumer<Player> click, Material material, Number data, String display, String... lore) {
        this(click, material, data, 1, display, lore);
    }
    
    private SpectatorItems(Consumer<Player> click, Material material, String display, String... lore) {
        this(click, material, 0, 1, display, lore);
    }
    
    public int index() {
        // 1, 3, 5, 7
        return 2 * ordinal() + 1;
    }
    
    public String getDisplay() {
        return display;
    }
    
    public String[] getLore() {
        return lore == null ? new String[0] : lore;
    }
    
    public Material getMaterial() {
        return material;
    }
    
    public Byte getData() {
        return data == null ? 0 : data.byteValue();
    }
    
    public Short getDurability() {
        return data == null ? 0 : data.shortValue();
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public void onClick(Player player) {
        if (click != null) {
            click.accept(player);
        }
    }
    
    
    public static SpectatorItems ofIndex(int index) {
        for (SpectatorItems item : values()) {
            if (item.index() == index) {
                return item;
            }
        }
        
        return null;
    }
    
    public static void giveTo(Player player) {
        for (SpectatorItems item : values()) {
            player.getInventory().setItem(item.index(), item.get());
        }
    }
}
