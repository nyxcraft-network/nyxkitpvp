package net.nyxcraft.dev.kitpvp.game;

import java.lang.ref.WeakReference;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import net.nyxcraft.dev.kitpvp.game.progression.ProgressionTree;
import net.nyxcraft.dev.kitpvp.util.PlayerStateChangeEvent;

import org.bukkit.entity.Player;

/**
 * Utility class for managing {@link PlayerData} for all players.
 * 
 * @author Gideon
 */
public class GameManager {

    private static GameManager instance = null;
    
    private final Map<String, PlayerData> players;
    
    private GameManager() {
        this.players = new LinkedHashMap<>();
    }
    
    public boolean hasPlayerData(Player player) {
        Objects.requireNonNull(player, "The given Player was null!");
        
        PlayerData data = getPlayerData(player);
        return data != null && data.isValid();
    }
    
    public PlayerData findPlayerData(Player player) {
        PlayerData data = getPlayerData(player);
        
        if (data == null) {
            data = addPlayerData(player);
        }
        
        return data;
    }
    
    public PlayerData getPlayerData(Player player) {
        Objects.requireNonNull(player, "The given Player was null!");
        
        return players.get(player.getName());
    }
    
    public PlayerData addPlayerData(Player player) {
        Objects.requireNonNull(player, "The given Player was null!");
        
        if (hasPlayerData(player)) {
            throw new IllegalStateException("The specified Player already has PlayerData!");
        }
        
        PlayerData data = new PlayerData(player);
        players.put(player.getName(), data);
        return data;
    }
    
    public PlayerData dropPlayerData(Player player) {
        Objects.requireNonNull(player, "The given Player was null!");
        
        return players.remove(player.getName());
    }
    
    public class PlayerData {
    
        private final WeakReference<Player> player;
        private final ProgressionTree tree;
        private PlayerState state;
        private Kit kit;
        private double gems;
        
        private PlayerData(Player player) {
            this.player = new WeakReference<>(player);
            this.tree = new ProgressionTree(this);
            this.state = PlayerState.SPECTATING;
            this.kit = new Kit();
            this.gems = 0d;
            
            PlayerStateChangeEvent.call(this, state, null);
        }
        
        public boolean equals(Object o) { 
            return o instanceof PlayerData && referenceEqual(player, ((PlayerData) o).player);
        }
        
        public int hashCode() {
            return referenceHash(player);
        }
        
        public String toString() {
            Player player = this.player.get();
            return isValid() ? "PlayerData representing " + player.getName() : "Invalid PlayerData";
        }
        
        public boolean isValid() {
            return player.get() != null;
        }
        
        public Player getPlayer() {
            return player.get();
        }
        
        public ProgressionTree getProgression() {
            return tree;
        }
        
        public PlayerState getState() {
            return state;
        }
        
        public void setSpectating() {
            setState(PlayerState.SPECTATING);
        }
        
        public void setFighting() {
            setState(PlayerState.FIGHTING);
        }
        
        public void setState(PlayerState state) {
            PlayerState previous = this.state;
            this.state = state;
            PlayerStateChangeEvent.call(this, state, previous);
        }
        
        public Kit getKit() {
            return kit;
        }
        
        public void setKit(Kit kit) {
            this.kit = Objects.requireNonNull(kit, "The given Kit cannot be null!");
        }
        
        public int getGems() {
            return (int) gems;
        }
        
        public void giveGems(double delta) {
            this.gems += delta;
        }
        
        public void setGems(double gems) {
            this.gems = gems;
        }
        
        public void drop() {
            if (!isValid()) {
                throw new IllegalStateException("This PlayerData is no longer valid!");
            }
            
            GameManager.this.dropPlayerData(getPlayer());
        }
    }
    
    
    public static GameManager getInstance() {
        if (instance == null) {
            instance = new GameManager();
        }
        
        return instance;
    }
    
    private static <T> boolean referenceEqual(WeakReference<T> a, WeakReference<T> b) {
        if (a == null ^ b == null) {
            return false;
        } else if (a != null && b != null) {
            return Objects.equals(a.get(), b.get());
        }
        
        return true;
    }
    
    public static int referenceHash(WeakReference<?> r) {
        return r == null ? 0 : Objects.hashCode(r.get());
    }
}
