package net.nyxcraft.dev.kitpvp.game;

import java.util.Iterator;

import net.nyxcraft.dev.kitpvp.KitPVP;
import net.nyxcraft.dev.kitpvp.game.config.GameConfig;
import net.nyxcraft.dev.kitpvp.game.config.MapConfig;
import net.nyxcraft.dev.kitpvp.util.MapChangeEvent;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;

import com.google.common.collect.Iterators;
import com.google.common.collect.Multisets;

/**
 * A class for managing configured maps and rotating through them.
 * 
 * @author Gideon
 */
public class MapRotation {

    private final String[] maps;
    private Iterator<String> it;
    private LoadedMap current;
    
    public MapRotation(GameConfig config) {
        this.maps = config.worlds == null ? new String[0] : config.worlds.clone();
        this.it = Iterators.forArray(maps);
        this.current = null;
    }
    
    private void tick() {
        if (current == null) {
            return;
        }
        
        final long remaining = current.unload - System.currentTimeMillis() / 1000;

        for (ScoreboardInstance instance : ScoreboardInstance.getScoreboards().values()) {
            if (remaining < 0) {
                instance.setField(instance.TIME, "Rotating");
            } else {
                instance.setField(instance.TIME, Long.toString(remaining) + "s");
            }
        }

        if (remaining < 0) {
            cycle();
        }
    }
    
    public int getMapCount() {
        return maps.length;
    }
    
    public boolean hasCurrentMap() {
        return current != null;
    }
    
    public LoadedMap getCurrentMap() {
        return current;
    }
    
    public MapRotation init() {
        if (current != null) {
            return this;
        }
        
        try {
            KitPVP.getInstance().getLogger().info("Initializing the MapRotation.");
            cycle();
            
            Bukkit.getScheduler().runTaskTimer(KitPVP.getInstance(), this::tick, 0L, 10L);
        } catch (IllegalStateException e) {
            KitPVP.getInstance().getLogger().severe("Failed to initialize MapRotation.");
            this.current = null;
        }
        
        return this;
    }
    
    @SuppressWarnings("deprecation")
    private LoadedMap cycle() {
        KitPVP.getInstance().getLogger().info("Cycling the MapRotation.");
        Iterator<String> it = Multisets.copyHighestCountFirst(ScoreboardInstance.getScores()).stream().distinct().iterator();
        Bukkit.broadcastMessage(ChatColor.GOLD + "The round has concluded!");
        
        for (int i = 0; i < 3 && it.hasNext(); i++) {
            String place;
            
            switch (i) {
            case 0:
                place = ChatColor.GOLD + "  1st";
                break;
            case 1:
                place = ChatColor.GRAY + "  2nd";
                break;
            case 2:
                place = ChatColor.BLUE + "  3rd";
                break;
            default:
                place = ChatColor.DARK_GRAY + "  ?";
                break;
            }
            
            String player = it.next(), score = Integer.toString(ScoreboardInstance.getScores().count(player));
            Bukkit.broadcastMessage(place + ": " + ChatColor.YELLOW + player + " with " + score + " kills.");
        }
        
        ScoreboardInstance.getScores().clear();
        for (ScoreboardInstance instance : ScoreboardInstance.getScoreboards().values()) {
            instance.updateScore();
        }
        
        for (Player player : Bukkit.getOnlinePlayers()) {
            GameManager.PlayerData data = GameManager.getInstance().getPlayerData(player);
            
            if (!PlayerState.SPECTATING.equals(data.getState())) {
                data.setSpectating();
            }

            player.setHealth(20.0);
            player.getActivePotionEffects().clear();
        }
        
        if (current != null && getMapCount() < 2) {
            KitPVP.getInstance().getLogger().info("Only one map found, recycling current map.");
            this.current = current.duplicate();
            return current;
        }
        
        LoadedMap previous = current;
        this.current = next();
        
        if (previous != null) {
            previous.unload(current);
        }
        
        MapChangeEvent.call(previous, current);
        return current;
    }
    
    private LoadedMap next() {
        World loaded = null;
        
        for (int i = 0; i < getMapCount(); i++) {
            if (it == null || !it.hasNext()) {
                it = Iterators.forArray(maps);
            }
            
            String worldname = it.next();
            loaded = Bukkit.createWorld(WorldCreator.name(worldname));
            
            if (loaded != null) {
                KitPVP.getInstance().getLogger().info("Loaded configured map of name '" + worldname + "'!");
                break;
            } else {
                KitPVP.getInstance().getLogger().warning("Unable to load configured map of name '" + worldname + "'!");
            }
        }
        
        if (loaded == null) {
            KitPVP.getInstance().getLogger().severe("Unable to successfully load a new world!");
            throw new IllegalStateException("This MapRotation could not load a map!");
        }
        
        return new LoadedMap(loaded, MapConfig.load(loaded));
    }
    
    
    private static void configure(World world) {
        world.setGameRuleValue("doMobSpawning", "false");
        world.setGameRuleValue("doDaylightCycle", "false");
        world.setPVP(true);
    }
    
    public static class LoadedMap {
    
        private final World world;
        private final MapConfig config;
        private final long unload;
        
        private LoadedMap(World world, MapConfig config) {
            this.world = world;
            this.config = config;
            this.unload = System.currentTimeMillis() / 1000 + config.seconds;
            
            configure(world);
        }
        
        public World getWorld() {
            return world;
        }
        
        public MapConfig getConfig() {
            return config;
        }
        
        public long getExpiry() {
            return unload;
        }
        
        public Location getRandomSpawn() {
            return config.getRandomSpawn(world);
        }
        
        private LoadedMap duplicate() {
            return new LoadedMap(world, config);
        }
        
        private void unload(LoadedMap into) {
            world.getPlayers().forEach(p -> p.teleport(into.getRandomSpawn()));
            Bukkit.unloadWorld(world, false);
        }
    }
}
