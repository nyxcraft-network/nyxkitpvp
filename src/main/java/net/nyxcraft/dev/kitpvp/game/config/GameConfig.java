package net.nyxcraft.dev.kitpvp.game.config;

import net.nyxcraft.dev.nyxutils.config.JsonConfig;

/**
 * Main plugin / game type configuration file template.
 * 
 * @author Gideon
 */
public class GameConfig extends JsonConfig {

    public String[] worlds = {};
}
