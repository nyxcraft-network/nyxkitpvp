package net.nyxcraft.dev.kitpvp.game.listener;

import net.md_5.bungee.api.ChatColor;
import net.nyxcraft.dev.kitpvp.KitPVP;
import net.nyxcraft.dev.kitpvp.game.GameManager;
import net.nyxcraft.dev.kitpvp.game.GameManager.PlayerData;
import net.nyxcraft.dev.kitpvp.game.MapRotation;
import net.nyxcraft.dev.kitpvp.game.PlayerState;
import net.nyxcraft.dev.kitpvp.game.ScoreboardInstance;
import net.nyxcraft.dev.kitpvp.game.SpectatorItems;
import net.nyxcraft.dev.kitpvp.util.PlayerUtil;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.player.Rank;
import net.nyxcraft.dev.nyxcore.utils.PlayerUtils;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;

/**
 * Listener for player-related events.
 * 
 * @author Gideon
 */
public class PlayerListener implements Listener {

    private final KitPVP plugin;
    private final GameManager gm;
    
    public PlayerListener() {
        this.plugin = KitPVP.getInstance();
        this.gm = GameManager.getInstance();
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void onPreLogin(AsyncPlayerPreLoginEvent e) {
        if (plugin.isSufficient() || Bukkit.getOfflinePlayer(e.getUniqueId()).isOp()) {
            return;
        }
        
        e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "An internal server error occurred.");
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onJoin(PlayerJoinEvent e) {
        e.getPlayer().setGameMode(GameMode.SURVIVAL);
        e.getPlayer().getActivePotionEffects().stream()
                .map(PotionEffect::getType)
                .forEachOrdered(e.getPlayer()::removePotionEffect);
        gm.addPlayerData(e.getPlayer());
        new ScoreboardInstance(e.getPlayer());
        MapRotation.LoadedMap map = plugin.getMapRotation().getCurrentMap();
        
        if (map != null) {
            e.getPlayer().teleport(map.getRandomSpawn());
        }
        
        SpectatorItems.KIT.onClick(e.getPlayer());
        Rank rank = NyxCore.getUser(e.getPlayer().getUniqueId()).getRank();
        String join = ChatColor.DARK_GRAY + "[" + rank.color + rank.prefix + ChatColor.DARK_GRAY +"] " + ChatColor.DARK_GRAY + e.getPlayer().getName() + " has joined.";
        e.setJoinMessage(join);
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void onQuit(PlayerQuitEvent e) {
        ScoreboardInstance.getScoreboards().remove(e.getPlayer().getUniqueId());
        gm.dropPlayerData(e.getPlayer());
        
        Rank rank = NyxCore.getUser(e.getPlayer().getUniqueId()).getRank();
        String quit = ChatColor.DARK_GRAY + "[" + rank.color + rank.prefix + ChatColor.DARK_GRAY +"] " + ChatColor.DARK_GRAY + e.getPlayer().getName() + " has left.";
        e.setQuitMessage(quit);
    }
    
    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        PlayerData data = gm.getPlayerData(e.getPlayer());
        
        if (data != null && PlayerState.SPECTATING.equals(data.getState())) {
            switch (e.getAction()) {
            case RIGHT_CLICK_AIR:
            case RIGHT_CLICK_BLOCK:
                break;
            default:
                return;
            }
            
            SpectatorItems item = SpectatorItems.ofIndex(e.getPlayer().getInventory().getHeldItemSlot());
            
            if (item != null) {
                e.setCancelled(true);
                item.onClick(e.getPlayer());
                e.getPlayer().updateInventory();
            }
        }
    }
    
    @EventHandler
    public void onPickupItem(PlayerPickupItemEvent e) {
        if (gm.hasPlayerData(e.getPlayer()) && PlayerState.FIGHTING.equals(gm.getPlayerData(e.getPlayer()).getState())) {
            return;
        }
        
        e.setCancelled(true);
    }
    
    @EventHandler
    public void onDropItem(PlayerDropItemEvent e) {
        if (gm.hasPlayerData(e.getPlayer()) && PlayerState.FIGHTING.equals(gm.getPlayerData(e.getPlayer()).getState())) {
            return;
        }
        
        e.setCancelled(true);
    }
    
    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        Player killer;
        
        if ((killer = getKiller(e)) != null) {
            ScoreboardInstance.incrementScore(killer.getUniqueId());
            gm.findPlayerData(killer).giveGems(.5);
        }
        
        MapRotation.LoadedMap current = KitPVP.getInstance().getMapRotation().getCurrentMap();
        Location spawn = current == null ? e.getEntity().getWorld().getSpawnLocation() : current.getRandomSpawn();
        PlayerUtil.respawn(e.getEntity(), spawn);
        
        PlayerData data = GameManager.getInstance().getPlayerData(e.getEntity());
        Bukkit.getScheduler().runTaskLater(KitPVP.getInstance(), data::setSpectating, 1L);
        
        PlayerUtils.clearArrowsFromPlayer(e.getEntity());
        e.getDrops().clear();
        e.setDeathMessage(ChatColor.DARK_GRAY + e.getDeathMessage());
        SpectatorItems.KIT.onClick(e.getEntity());
    }
    
    @EventHandler
    public void onHunger(FoodLevelChangeEvent e) {
        e.setCancelled(true);
    }
    
    
    private static Player getKiller(PlayerDeathEvent event) {
        Player p = event.getEntity();
        
        if (p.isDead()) {
            if (p.getKiller() instanceof Player) {
                return (Player) p.getKiller();
            }
        }
        
        return null;
    }
}
