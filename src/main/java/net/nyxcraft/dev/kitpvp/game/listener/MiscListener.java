package net.nyxcraft.dev.kitpvp.game.listener;

import net.nyxcraft.dev.kitpvp.game.MapRotation;
import net.nyxcraft.dev.kitpvp.game.ScoreboardInstance;
import net.nyxcraft.dev.kitpvp.util.MapChangeEvent;
import net.nyxcraft.dev.nyxcore.NyxCore;
import net.nyxcraft.dev.nyxcore.player.Rank;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Listener for miscellaneous events.
 * 
 * @author Gideon
 */
public class MiscListener implements Listener {
    
    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        Rank rank = NyxCore.getUser(e.getPlayer().getUniqueId()).getRank();
        String format = rank.color + rank.prefix + ChatColor.DARK_GRAY + "\\\\";
        
        if (rank.ordinal() >= Rank.HELPER.ordinal()) {
            format += ChatColor.WHITE + " %s: %s";
        } else {
            format += ChatColor.GRAY + " %s: %s";
        }
        
        e.setFormat(format);
    }
    
    @EventHandler
    public void onMapChange(MapChangeEvent e) {
        MapRotation.LoadedMap current = e.getCurrent();
        boolean isnull = current == null || current.getConfig() == null;
        
        String mapname = isnull ? null : current.getConfig().getName();
        String mapauthor = isnull ? null : current.getConfig().getAuthor();

        for (ScoreboardInstance instance : ScoreboardInstance.getScoreboards().values()) {
            instance.setField(instance.MAPNAME, mapname);
            instance.setField(instance.MAPAUTHOR, mapauthor);
        }
    }
    
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (Material.TNT.equals(e.getBlockPlaced().getType())) {
            Location tnt = e.getBlockPlaced().getLocation();
            TNTPrimed primed = (TNTPrimed) tnt.getWorld().spawnEntity(tnt, EntityType.PRIMED_TNT);
            primed.setFuseTicks(20);
            e.getPlayer().getInventory().removeItem(new ItemStack(Material.TNT));
        }
        
        e.setCancelled(true);
    }
    
    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        e.setCancelled(true);
    }
    
    @EventHandler
    public void onWeatherChange(WeatherChangeEvent e) {
        e.setCancelled(true);
    }
    
    @EventHandler
    public void onEntityExplode(EntityExplodeEvent e) {
        e.blockList().clear();
        e.setYield(0f);
    }
}
