package net.nyxcraft.dev.kitpvp.game.progression.passive;

import java.util.Iterator;

import net.nyxcraft.dev.kitpvp.KitPVP;
import net.nyxcraft.dev.kitpvp.game.progression.Passive;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

// *Rage: Hold block for a 3 seconds > Explosion around player, and speed +2/resistance +2 for 10 seconds.   90 sec cooldown
public class Rage extends Passive {

    private static final PotionEffect SPEED = new PotionEffect(PotionEffectType.SPEED, 200, 1, true);
    private static final PotionEffect RESIST = new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 200, 1, true);
    
    private final BukkitTask task;
    private final Multiset<String> progress;
    private final Multiset<String> cooldown;
    
    public Rage() {
        super(null, null);
        
        this.task = Bukkit.getScheduler().runTaskTimer(KitPVP.getInstance(), this::check, 10L, 10L);
        this.progress = HashMultiset.create();
        this.cooldown = HashMultiset.create();
    }
    
    public void stop() {
        try {
            task.cancel();
        } catch (Throwable t) {
            //
        }
    }
    
    private void play(Player player) {
        // Cooldown of 90 seconds, ticked twice per second
        cooldown.setCount(player.getName(), 180);
        
        // Play effect
        final Location loc = player.getEyeLocation();
        player.getWorld().createExplosion(loc.getX(), loc.getY(), loc.getZ(), 4f, false, false);
        player.addPotionEffect(SPEED);
        player.addPotionEffect(RESIST);
    }
    
    @EventHandler
    public void onExplosion(EntityDamageByEntityEvent e) {
        switch (e.getCause()) {
            case BLOCK_EXPLOSION:
            case ENTITY_EXPLOSION:
                break;
            default:
                return;
        }
        
        // Players are immune to explosions for ~1 second after the effect as otherwise they'd hurt themselves
        if (e.getEntity() instanceof Player && cooldown.count(((Player) e.getEntity()).getName()) >= 89) {
            e.setCancelled(true);
        }
    }
    
    private void check() {
        for (Iterator<Multiset.Entry<String>> it = progress.entrySet().iterator(); it.hasNext(); ) {
            final Player player = Bukkit.getPlayer(it.next().getElement());
            
            if (player == null) {
                it.remove();
                continue;
            } else if (!player.isSneaking()) {
                it.remove();
                continue;
            } else if (!active(player)) {
                it.remove();
                continue;
            }
        }
        
        for (String name : cooldown.stream().toArray(String[]::new)) {
            final Player player = Bukkit.getPlayer(name);
            
            if (player == null) {
                continue;
            }
            
            cooldown.remove(name);
        }
        
        for (Player player : super.players) {
            if (player.isSneaking() && !cooldown.contains(player.getName())) {
                progress.add(player.getName());
            }
        }
        
        for (Iterator<Multiset.Entry<String>> it = progress.entrySet().iterator(); it.hasNext(); ) {
            final Multiset.Entry<String> entry = it.next();
            final Player player = Bukkit.getPlayer(entry.getElement());
            
            if (player == null) {
                it.remove();
                continue;
            } else if (entry.getCount() >= 6) {
                it.remove();
                play(player);
                continue;
            }
        }
    }
}
