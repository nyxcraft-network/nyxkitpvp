package net.nyxcraft.dev.kitpvp.db;

import net.nyxcraft.dev.kitpvp.db.dao.KitPVPDataDAO;
import net.nyxcraft.dev.ndb.NyxDB;
import net.nyxcraft.dev.ndb.mongodb.ResourceManager;

import org.mongodb.morphia.Datastore;

public class KitPVPDatabaseManager {

    private ResourceManager rscman;
    private Datastore datastore;

    public KitPVPDatabaseManager() {
        this.rscman = NyxDB.getResourceManager();
        this.datastore = rscman.getDatastore("net.nyxcraft.dev.nlby.database.entities");
        
        datastore.ensureCaps();
        datastore.ensureIndexes();
        
        new KitPVPDataDAO(datastore);
    }
}
